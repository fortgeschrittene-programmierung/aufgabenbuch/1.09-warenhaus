# 1.05 Warenhaus

**Voraussetzung**
Vorlesung zum Thema generische Klassen, Methoden (Generics).

**Ziel**
Einüben der Erstellung und Verwendung von generischen Klassen.

**Dauer**
< 1 Stunde

## Aufgabenstellung
In einer Warenhaussoftware sollen Produkte verwaltet werden. In der vorliegenden Übungsaufgabe sollen die grundlegenden Typen entwickelt werden.

### (a) Generische Klasse `Product`
Schreiben Sie eine generische Klasse `Product`, die ein Produkt repräsentiert. Ein `Product`-Objekt besteht aus zwei Attributen:
- Das erste Attribut ist die Produktbeschreibung, deren tatsächliche Klasse erst bei der Verwendung der Klasse festgelegt werden soll. Definieren Sie deshalb stattdessen eine Typvariable (Stellvertreterbuchstabe).
- Das zweite Attribut ist der Produktpreis und vom Datentyp `double`.

Die Klasse `Product` soll einen Konstruktor besitzen, der Werte für beide Attribute als Argumente übergeben bekommt.

Des Weiteren programmieren Sie eine ausführbare Klasse `App`, welche eine Anzahl von `Product`-Objekten erzeugt (verwenden Sie zunächst die Klasse `String` als Typ für die Produktbeschreibung), in einer `ArrayList` speichert und auf der Standardausgabe ausgibt. Verwenden Sie zur Ausgabe der Liste die „for-each“-Schleife von Java über die Liste. Achten Sie darauf, dass Variablen der generischen Klasse `Product` immer vollständig deklariert sind (also mit Angabe der konkreten Klasse, die für die Typvariable verwendet werden soll)!

### (b) Klasse `ProductDescription`
Schreiben Sie eine Klasse `ProductDescription`, die eine Produktbeschreibung repräsentiert. Die Klasse hat zwei Attribute:
- Eine Produktgruppe
- Einen Beschreibungstext

Beide Attribute werden durch die Klasse `String` repräsentiert. Implementieren Sie alle nötigen Konstruktoren und die Methode `public String toString()`, deren Ergebnis-String die Produktgruppe und den Text enthalten soll. Verändern Sie die Klasse `App` aus Teilaufgabe (a) so, dass anstelle eines `String`-Objekts ein Objekt der Klasse `ProductDescription` als Produktbeschreibung verwendet wird.

### (c) Preisstufen und Enum `PriceLevel`
Das Preissystem der Warenhaussoftware soll umgestellt werden. Produktpreise sollen nicht mehr beliebig vergeben werden können. Stattdessen gibt es vier Preisstufen, die einem Produkt zugeordnet werden können:
- `LOW` (9,99)
- `MEDIUM` (19,99)
- `HIGH` (49,99)
- `EXCLUSIVE` (99,99)

Innerhalb der Warenhaussoftware sollen die Preisstufen durch einen Enum-Typ (`enum PriceLevel`) repräsentiert werden. Programmieren Sie den Enum `PriceLevel`. Passen Sie die Klasse `Product` und Ihre Klasse `App` aus Teilaufgabe (a) entsprechend an.

**Hinweis:** Der Preis muss als `double`-Datentyp im Enum `PriceLevel` verwaltet werden. Der Enum-Typ muss zusätzlich die Methode `public String toString()` überschreiben (gibt den Preis als `String` zurück) und einen privaten Konstruktor besitzen, der den Preis als Argument erhält (siehe Folienskript "Aufzählungstypen / Enums").
